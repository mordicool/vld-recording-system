/**
 * Created by מרדכי on 02 ינואר 2017.
 */

var config = require('../config');
var fileHandler = require('../modules/uploadFile/fileHandler');
var logger = require('../modules/logger');
var router = require('express').Router();
const path = require('path');

router.get('/', (req, res) => {
    logger.debug('User entered upload page', req.user.guid);
    return res.sendFile('public/pages/upload.html', {root: path.join(__dirname, '../')});
});
router.post('/', uploadFile);

module.exports = router;

/********************************************************************************************/

function uploadFile(req, res) {
    fileHandler.handleFile(req)
        .then(function (filePath) {
            logger.info('New record arrived. File name: ' + filePath);
            res.sendStatus(200);
        })
        .fail(function (error) {
            logger.error('Error while receiving a record. Error message: ' + error.message);
            res.sendStatus(500);
        });
}