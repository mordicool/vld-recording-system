var api = require('../modules/db/publicStoredValues/api');
var adminRoute = require('./admin');
var config = require('../config');
var ensureLogin = require('connect-ensure-login');
var logger = require('../modules/logger');
var router = require('express').Router();
var recordingsViewRoute = require('./recordingsView');
var uploadRoute = require('./upload');
var usersRoute = require('./users');

router.get('/', serveIndexPage);
router.get('/getTree', ensureLogin.ensureLoggedIn(), serveTree);
router.get('/getLecturers', ensureLogin.ensureLoggedIn(), serveLecturers);
router.use('/admin', ensureLogin.ensureLoggedIn(), ensureType('ADMIN'), adminRoute);
router.use('/recordingsView', ensureLogin.ensureLoggedIn(), recordingsViewRoute);
router.use('/upload', ensureLogin.ensureLoggedIn(), ensureType('UPLOAD', 'ADMIN'), uploadRoute);
router.use('/', usersRoute);

module.exports = router;

/********************************************************************************************/

function serveIndexPage(req, res) {
    const type = req.user === undefined ? undefined : req.user.type;
    switch (type) {
        case 'ADMIN':
            return res.redirect('/admin');
        case 'UPLOAD':
            return res.redirect('/upload');
        case 'DOWNLOAD':
            return res.redirect('/recordingsView');
        default:
            return res.redirect('/login');
    }
}

function serveTree(req, res) {
    api.getTree()
        .then(function (treeDocument) {
            res.status(200).json(treeDocument.value);
        })
        .fail(function () {
            res.sendStatus(500);
        })
}

function serveLecturers(req, res) {
    api.getLecturers()
        .then(function (lecturersDocument) {
            res.status(200).json(lecturersDocument.value.sort());
        })
        .fail(function () {
            res.sendStatus(500);
        })
}

function ensureType(...types) {
    return function (req, res, next) {
        if (!types.find(type => req.user.type === type)) {
            return res.redirect('/');
        }
        next();
    };
}