var api = require('../modules/db/users/api');
var config = require('../config');
var logger = require('../modules/logger');
var ensureLogin = require('connect-ensure-login');
var router = require('express').Router();
const path = require('path');

const passport = require('passport'),
    {Strategy} = require('passport-local').Strategy;

passport.use(new Strategy((username, password, cb) => {
    api.authenticateUser(username, password)
        .then(user => {
            logger.info('User logged in', user.guid);
            cb(null, user)
        });
}));
passport.serializeUser((user, cb) => cb(null, user.guid));
passport.deserializeUser((id, cb) => {
    api.getUserById(id).then(user => cb(null, user));
});

router.post('/login', ensureLogin.ensureLoggedOut(), passport.authenticate('local', { 'successReturnToOrRedirect': '/', failureRedirect: '/login' }));
router.get('/login', ensureLogin.ensureLoggedOut(), (req, res) => {
    logger.debug('User entered login page');
    return res.sendFile('public/pages/login.html', {root: path.join(__dirname, '../')});
});
router.get('/logout', ensureLogin.ensureLoggedIn(), (req, res) => {
    logger.info('User logged out', req.user.guid);
    req.logout();
    res.redirect('/');
});

module.exports = router;