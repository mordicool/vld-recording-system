/**
 * Created by מרדכי on 06 ינואר 2017.
 */

var api = require('../../modules/db/publicStoredValues/api');
var config = require('../../config');
var fs = require('fs');
var logger = require('../../modules/logger');
var path = require('path');
var router = require('express').Router();

router.post('/updateTree', updateTree);

module.exports = router;

/********************************************************************************************/

function updateTree(req, res) {
    var newTree = req.body.data;
    api.changeTree(newTree)
        .then(function () {
            logger.info('Tree changed successfully.');
            res.sendStatus(200);
        })
        .fail(function () {
            res.sendStatus(500);
        })
}