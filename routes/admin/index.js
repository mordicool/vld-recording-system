var lecturersRoute = require('./lecturers');
var userManagement = require('./userManagement');
var jstreeRoute = require('./jstree');
var logsViewRoute = require('./logsView');
var logger = require('../../modules/logger');
var router = require('express').Router();
const path = require('path');

router.get('/', (req, res) => {
    logger.debug('User entered admin page');
    return res.sendFile('public/pages/admin.html', {root: path.join(__dirname, '../../')});
});
router.use('/lecturers', lecturersRoute);
router.use('/userManagement', userManagement);
router.use('/jstree', jstreeRoute);
router.use('/logs', logsViewRoute);

module.exports = router;