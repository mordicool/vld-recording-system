var bodyParser = require('body-parser');
var config = require('./config');
var cookieParser = require('cookie-parser');
var dbConnection = require('./modules/db/dbConnection');
var express = require('express');
var logger = require('./modules/logger');
var router = require('./routes');
const passport = require('passport');

var app = express();
app.use(require('compression')());
app.use(cookieParser('mordicool'));
app.use(bodyParser.json());
app.use(require('express-session')({ secret: config.session.secret, resave: false, saveUninitialized: false }));
app.use(express.static(__dirname + '/public'));
app.use(passport.initialize());
app.use(passport.session());

app.use('/', router);

var port = process.env.PORT || config.server.port;
app.listen(port, '0.0.0.0', function () {
    logger.info('Server is listening on port ' + port);
});

dbConnection.connect();